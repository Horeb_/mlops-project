from flask import Flask, request, jsonify
import joblib
import string
import pandas as pd

model_regr = joblib.load('model.v1.bin')

app = Flask(__name__)

def encode_prenom (prenom : str) -> pd.Series:
    alphabet = string.ascii_lowercase + "-'"
    prenom = prenom.lower()
    return pd.Series([leter in prenom for leter in alphabet]).astype(int)

@app.route('/predict', methods=['GET'])
def predict_gender_route():
    name = request.args.get('prenom')
    if name : 
        encoded_prenom = encode_prenom(name)

        gender = model_regr.predict([encoded_prenom])
        
        gender = "Homme" if gender.tolist()[0] ==0 else "Femme"
        response = {
            "name" : name,
            "gender" : gender,
        }
        return jsonify(response)
    else : 
        return jsonify({"error" : "You need to enter a name as GET parameter"})
