FROM python:3.8

RUN curl -sSL https://install.python-poetry.org | python3 -
RUN mkdir /app
WORKDIR /app
COPY . .
ENV POETRY_VIRTUALENVS_CREATE=false
RUN $HOME/.local/bin/poetry install
EXPOSE 5000
CMD ["flask", "--app", "api", "run","--debug", "--host", "0.0.0.0"]